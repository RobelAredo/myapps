package com.example.feature_bottoms_up.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.feature_bottoms_up.model.BottomsUpRepo
import com.example.feature_bottoms_up.model.response.CategoryDTO
import com.example.feature_bottoms_up.view.category.CategoryState
import kotlinx.coroutines.launch

class CategoryViewModel: ViewModel() {
    private val repo by lazy {  BottomsUpRepo }

    private val _state = MutableLiveData<CategoryState<String>>(CategoryState((true)))
    val state: LiveData<CategoryState<String>> get() = _state

    init {
        viewModelScope.launch {
            val categoryDTO = repo.getCategories()
            _state.value = CategoryState(categories = categoryDTO)
        }
    }
}