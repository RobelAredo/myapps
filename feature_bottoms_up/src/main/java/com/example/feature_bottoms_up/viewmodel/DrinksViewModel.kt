package com.example.feature_bottoms_up.viewmodel

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.feature_bottoms_up.R
import com.example.feature_bottoms_up.databinding.ItemBinding
import com.example.feature_bottoms_up.model.BottomsUpRepo
import com.example.feature_bottoms_up.model.response.CategoryDrinksDTO
import com.example.feature_bottoms_up.view.category.CategoryState
import kotlinx.coroutines.launch

class DrinksViewModel(category: String) : ViewModel() {
    private val repo by lazy { BottomsUpRepo }

    private val _state =
        MutableLiveData<CategoryState<CategoryDrinksDTO.Drink>>(CategoryState((true)))
    val state: LiveData<CategoryState<CategoryDrinksDTO.Drink>> get() = _state

    init {
        viewModelScope.launch {
            val drinksDTO = repo.getDrinks(category)
            _state.value = CategoryState(categories = drinksDTO)
        }
    }
}