package com.example.feature_bottoms_up.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import kotlin.reflect.KFunction

class GenericViewModelFactory(
    private val viewModel: ViewModel
): ViewModelProvider.NewInstanceFactory() {

    override fun <T:ViewModel?> create(modelClass: Class<T>): T =
        viewModel as T
}