package com.example.feature_bottoms_up.view.category

data class CategoryState<T> (
    val isLoading: Boolean = false,
    val categories: List<T> = emptyList()
)
