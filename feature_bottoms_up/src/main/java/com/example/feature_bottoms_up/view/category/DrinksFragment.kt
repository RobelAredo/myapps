package com.example.feature_bottoms_up.view.category

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.feature_bottoms_up.databinding.FragmentDrinksBinding
import com.example.feature_bottoms_up.databinding.ItemDrinkBinding
import com.example.feature_bottoms_up.model.adapter.GenericAdaptor
import com.example.feature_bottoms_up.model.response.CategoryDrinksDTO
import com.example.feature_bottoms_up.viewmodel.DrinksViewModel
import com.example.feature_bottoms_up.viewmodel.GenericViewModelFactory
import com.squareup.picasso.Picasso

class DrinksFragment: Fragment() {
    private var _binding: FragmentDrinksBinding? = null
    private val binding get() = _binding!!

    private val drinksViewModel by viewModels<DrinksViewModel>{
        GenericViewModelFactory(DrinksViewModel(args.category))
    }
    private val args by navArgs<DrinksFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDrinksBinding.inflate(inflater, container, false).also {
            _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        drinksViewModel.state.observe(viewLifecycleOwner){ drinks ->
            binding.piLoadingCategories.isVisible = drinks.isLoading
            binding.rvDrinks.adapter =
                GenericAdaptor(drinks.categories,
                    ::loadDrinkData, ::itemInflater, ::navigate).apply { add() }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun navigate(id: Int) {
        findNavController().navigate(DrinksFragmentDirections.actionDrinksFragmentToDrinkDetailsFragment(id))
    }

    private fun loadDrinkData(drink: CategoryDrinksDTO.Drink, navigate: (Int) -> Unit, binding: ItemDrinkBinding) {
        binding.tvDrinkId.text = drink.idDrink
        binding.tvDrink.text = drink.strDrink
        Picasso.get().load(drink.strDrinkThumb).into(binding.ivDrinkThumb)
        binding.cvDrinkDetails.setOnClickListener{
            navigate(drink.idDrink.toInt())
        }
    }

    private fun itemInflater (parent: ViewGroup): ItemDrinkBinding {
        return ItemDrinkBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    }
}