package com.example.feature_bottoms_up.model.DI

import android.content.Context
import com.example.booknerd.model.response.BottomsUpModule
import com.example.feature_bottoms_up.view.category.DrinksFragment
import com.example.myapps.ModuleDependencies
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Component(modules = [BottomsUpModule::class, ModuleDependencies::class])
@Singleton
interface BottomsUpDataComponent {
    fun inject(fragment : DrinksFragment)

    @Component.Builder
    interface Builder{
        fun context(@BindsInstance context: Context): Builder
        fun appDependencies(moduleDependencies: ModuleDependencies): Builder
        fun bottomsUpModule(bottomsUpModule: BottomsUpModule): Builder
        fun build(): BottomsUpDataComponent
    }
}