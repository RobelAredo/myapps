package com.example.feature_bottoms_up.model

import com.example.feature_bottoms_up.model.remote.BottomsUpService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object BottomsUpRepo {

    private val bottomsUpService by lazy { BottomsUpService.getInstance() }

    suspend fun getCategories() = withContext(Dispatchers.IO) {
        bottomsUpService.getCategories().categoryItem.map{it.strCategory}
    }

    suspend fun getDrinks(category: String) = withContext(Dispatchers.IO) {
        bottomsUpService.getDrinksInCategory(category = category).drinks
    }

    suspend fun getDrinkDetails(drinkId: Int) = withContext(Dispatchers.IO) {
        bottomsUpService.getDrinkDetails(drinkId).drinks
    }
}