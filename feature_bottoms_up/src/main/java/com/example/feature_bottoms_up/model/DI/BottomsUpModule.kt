package com.example.booknerd.model.response

import com.example.feature_bottoms_up.model.remote.BottomsUpService
import com.example.myapps.RetrofitStore
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
class BottomsUpModule {

    private val BASE_URL = "https://www.thecocktaildb.com"

    @Provides
    fun providesInstance(@RetrofitStore retrofit: Retrofit): BottomsUpService =
        retrofit.create(BottomsUpService::class.java)

}
