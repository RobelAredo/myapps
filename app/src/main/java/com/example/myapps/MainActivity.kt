package com.example.myapps

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.myapps.databinding.ActivityMainBinding
import com.example.random_color.MyColor

class MainActivity : AppCompatActivity() {

    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    private val navHostFragment by lazy {
        supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
    }
//    private val navController by lazy { navHostFragment.navController }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
//        binding.bottomNavigation.setOnNavigationItemReselectedListener { item ->
//            when(item.itemId) {
//                R.id.shibe_item -> findNavController().navigate()
//            }
//        }

//        setupActionBarWithNavController(navController, AppBarConfiguration(setOf(R.id.shibeFragment, R.id.categoryFragment, R.id.testFragment)))
        binding.bottomNavigation.setupWithNavController(navHostFragment.navController)
    }
}