package com.example.feature_shibe.utils

import coil.load
import com.bumptech.glide.Glide
import com.squareup.picasso.Picasso
import com.google.android.material.imageview.ShapeableImageView

enum class LoadImage {
    loadWithPicasso {
        override fun loadImage(shibeUrl: String, iv: ShapeableImageView) {
            Picasso.get().load(shibeUrl).into(iv)
        }
    },
    loadWithGlide {
        override fun loadImage(shibeUrl: String, iv: ShapeableImageView) {
            Glide.with(iv.context).load(shibeUrl).into(iv)
        }
    },
    loadWithCoil {
        override fun loadImage(shibeUrl: String, iv: ShapeableImageView) {
            iv.load(shibeUrl)
        }
    };

    abstract fun loadImage(shibeUrl: String, iv: ShapeableImageView)
}