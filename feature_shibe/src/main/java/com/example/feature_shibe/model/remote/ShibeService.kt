package com.example.feature_shibe.model.remote

import com.example.feature_shibe.model.local.entity.Shibe
import com.example.feature_shibe.model.response.ShibeDTO
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ShibeService {
    companion object {
        private const val BASE_URL = "https://shibe.online"
        fun getInstance(): ShibeService {
            return Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build().let{ it.create(ShibeService::class.java)}
        }
    }

//    shibe.online/api/shibes?count=
    @GET("/api/shibes")
    suspend fun getShibes(@Query("count") count: Int = 100): List<String>
}