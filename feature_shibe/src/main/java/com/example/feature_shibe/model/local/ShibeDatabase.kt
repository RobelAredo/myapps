package com.example.feature_shibe.model.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.feature_shibe.model.local.dao.FavoritesDao
import com.example.feature_shibe.model.local.dao.ShibeDao
import com.example.feature_shibe.model.local.entity.Favorite
import com.example.feature_shibe.model.local.entity.Shibe

@Database(entities = [Shibe::class, Favorite::class], version = 1)
abstract class ShibeDatabase: RoomDatabase() {

    abstract fun shibeDao(): ShibeDao
    abstract fun favorites(): FavoritesDao

    companion object {

        private const val DATABASE_NAME = "shibe.db"
//        private const val DATABASE_FAVORITES_NAME = "favorites.db"


        @Volatile
        private var instance: ShibeDatabase? = null

        fun getInstance(context: Context): ShibeDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        private fun buildDatabase(context: Context): ShibeDatabase {
            return Room.databaseBuilder(context, ShibeDatabase::class.java, DATABASE_NAME).build()
        }
    }
}