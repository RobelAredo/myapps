package com.example.feature_shibe.model.local.dao

import androidx.room.*
import com.example.feature_shibe.model.local.entity.Shibe

@Dao
interface ShibeDao {

    @Query("SELECT * FROM shibe")
    suspend fun getAll(): List<Shibe>

    @Insert
    suspend fun insert(shibes: List<Shibe>)

    @Update
    suspend fun update(shibe: Shibe)
}