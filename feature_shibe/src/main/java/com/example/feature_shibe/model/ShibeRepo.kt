package com.example.feature_shibe.model

import android.content.Context
import com.example.feature_shibe.model.local.ShibeDatabase
import com.example.feature_shibe.model.local.entity.Shibe
import com.example.feature_shibe.model.remote.ShibeService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ShibeRepo(val context: Context) {

//    private val context = ShibeFragment.ServiceLocator.getContext()

    private val shibeService by lazy { ShibeService.getInstance() }
    val database = ShibeDatabase.getInstance(context)
    val shibeDao = database.shibeDao()
    val favoritesDao = database.favorites()

    suspend fun getShibes() = withContext(Dispatchers.IO) {
        val cacheShibes: List<Shibe> = shibeDao.getAll()

        if (cacheShibes.size != 0) {
            return@withContext cacheShibes
        } else {
            val shibes: List<Shibe> = shibeService.getShibes().map{ Shibe(url = it) }
            shibeDao.insert(shibes)
            return@withContext shibes
        }
    }

    suspend fun getFavorites() = withContext(Dispatchers.IO) {
        favoritesDao.getFavorites().map {Shibe(it.id, it.url, it.liked)}
    }
}