package com.example.feature_shibe.model.local.dao

import androidx.room.*
import com.example.feature_shibe.model.local.entity.Favorite
import com.example.feature_shibe.model.local.entity.Shibe

@Dao
interface FavoritesDao {
    @Query("SELECT * FROM favorite")
    suspend fun getFavorites(): List<Favorite>

    @Insert
    suspend fun insertFavorite(favorite: Favorite)

    @Delete
    suspend fun delete(favorite: Favorite)
}