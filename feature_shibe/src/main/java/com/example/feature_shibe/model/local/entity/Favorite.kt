package com.example.feature_shibe.model.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Favorite (
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val url: String,
    var liked: Boolean = false
    )