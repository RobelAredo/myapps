package com.example.feature_shibe.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.constraintlayout.widget.Constraints
import androidx.core.view.isVisible
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.feature_shibe.databinding.FragmentShibeBinding
import com.example.feature_shibe.databinding.ItemShibeBinding
import com.example.feature_shibe.model.ShibeRepo
import com.example.feature_shibe.model.local.entity.Favorite
import com.example.feature_shibe.model.local.entity.Shibe
//import com.example.feature_shibe.utils.LoadImage
import com.google.android.material.imageview.ShapeableImageView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ShibeAdapter(private val repo: ShibeRepo, private val fragment: FragmentShibeBinding): RecyclerView.Adapter<ShibeAdapter.ShibeViewHolder>() {

    private var shibes = mutableListOf<Shibe>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ShibeViewHolder(fragment, repo,
        ItemShibeBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ShibeViewHolder, position: Int) {
        holder.loadShibes(shibes[position])
    }

    override fun getItemCount(): Int {
        return shibes.size
    }

    fun changeList(shibes: List<Shibe>) {
        this.shibes = shibes.toMutableList()
        notifyDataSetChanged()
    }

    class ShibeViewHolder (val fragment: FragmentShibeBinding, val repo:  ShibeRepo, val binding: ItemShibeBinding): RecyclerView.ViewHolder(binding.root) {

        fun loadShibes(shibe: Shibe) {
            with(binding){
                laHeart.isVisible = shibe.liked

                with(ivShibe){
//                    size = if (size > 300) 250 else 500

                    Glide.with(context).load(shibe.url).into(this)
//                    this.load(shibe.url)
//                    Picasso.get().load(shibe.url).into(this)
//                    LoadImage.valueOf("Picasso").loadImage(shibe.url, this)
//                    if(fragment.rvShibes.layoutManager is GridLayoutManager) {
//                        ivShibe.layoutParams.height = Constraints.LayoutParams.MATCH_PARENT
//                        LoadImage.valueOf("loadWithGlide").loadImage(shibe.url, this)
//                    }
//                    else {
//                        ivShibe.layoutParams.height = Constraints.LayoutParams.WRAP_CONTENT
//                        LoadImage.valueOf("loadWithGlide").loadImage(shibe.url, this)
//                    }

                    setOnClickListener {
                        with(binding.laHeart) {
                            isVisible = !isVisible
                            shibe.liked = isVisible
                            if(isVisible) playAnimation()
                            CoroutineScope(Dispatchers.IO).launch {
                                repo.shibeDao.update(shibe)
                                if (shibe.liked) repo.favoritesDao.insertFavorite(Favorite(id=shibe.id, url = shibe.url))
                                else repo.favoritesDao.delete(Favorite(id=shibe.id, url = shibe.url))
                            }
                            Log.d("LOGGER", "HEART visible = $isVisible")
                        }
                    }
                }
            }
        }


        private var ShapeableImageView.size
            get() = height
            set(len) { layoutParams.height = len }
    }
}