package com.example.feature_shibe.view

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.feature_shibe.R
import com.example.feature_shibe.databinding.FragmentShibeBinding
import com.example.feature_shibe.adapter.ShibeAdapter
import com.example.feature_shibe.model.ShibeRepo
import com.example.feature_shibe.model.response.ShibeDTO
import com.example.feature_shibe.viewmodel.ShibeViewModel

class ShibeFragment: Fragment() {
    private var _binding: FragmentShibeBinding? = null
    private val binding get() = _binding!!
    private val repo by lazy { ShibeRepo(requireContext()) }
    private val shibeViewModel by viewModels<ShibeViewModel>(){
        ShibeViewModel.ShibeViewModelFactory(repo)
    }

//    object ServiceLocator {
//        fun getContext(): Context = this.getContext()
//    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentShibeBinding.inflate(inflater, container, false).also{
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        val spinner: Spinner = binding.sOptions
//// Create an ArrayAdapter using the string array and a default spinner layout
//        this.context?.let {
//            ArrayAdapter.createFromResource(
//                it,
//                R.array.options,
//                android.R.layout.simple_spinner_item
//            ).also { adapter ->
//                // Specify the layout to use when the list of choices appears
//                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//                // Apply the adapter to the spinner
//                spinner.adapter = adapter
//            }
//        }

//        shibeViewModel.context = this.requireContext()
        var favoritesDisplay = false

        with(binding) {
            Log.d("LOGGER", R.array.options.toString())
            sOptions.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {

                }

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    rvShibes.layoutManager =
                        with (p0?.getItemAtPosition(p2).toString()) {
                        when {
                            this == "Linear" -> LinearLayoutManager(context)
                            this == "Grid" -> GridLayoutManager(context, 3)
                            else -> StaggeredGridLayoutManager(3, 1)
                        }
                    }
                    if (favoritesDisplay) shibeViewModel.getFavorites()
                    else shibeViewModel.getShibes()
                }
            }

            shibeViewModel.state.observe(viewLifecycleOwner){ state ->
                rvShibes.adapter = ShibeAdapter(repo, binding).apply { changeList(state.shibes) }
            }


            favorites.setOnClickListener {
                with(shibeViewModel){
                    favoritesDisplay = !favoritesDisplay
                    if (favoritesDisplay) {
                        favorites.setColorFilter(Color.RED)
                        getFavorites()
                    } else {
                        favorites.setColorFilter(Color.WHITE)
                        getShibes()
                    }
                }

            }
        }




    }
}