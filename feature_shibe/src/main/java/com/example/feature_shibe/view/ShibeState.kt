package com.example.feature_shibe.view

import com.example.feature_shibe.model.local.entity.Shibe
import com.example.feature_shibe.model.response.ShibeDTO

class ShibeState (
    val isLoading: Boolean = false,
    val shibes: List<Shibe> = listOf())