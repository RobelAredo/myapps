package com.example.feature_shibe.viewmodel

import android.content.Context
import androidx.lifecycle.*
import com.example.feature_shibe.model.ShibeRepo
import com.example.feature_shibe.view.ShibeState
import kotlinx.coroutines.launch

class ShibeViewModel(val repo: ShibeRepo): ViewModel() {
//    lateinit var context: Context
//    private val repo = ShibeRepo(context)

    private var _state = MutableLiveData(ShibeState(true))
    val state: LiveData<ShibeState> get() = _state

    init {
        getShibes()
    }

    fun getShibes() {
//        repo.context = context
        viewModelScope.launch {
            _state.value = ShibeState(shibes = repo.getShibes())
        }
    }

    fun getFavorites(){
        viewModelScope.launch {
            _state.value = ShibeState(shibes = repo.getFavorites())
        }
    }

    class ShibeViewModelFactory(
        private val repo: ShibeRepo
    ) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ShibeViewModel(repo) as T
        }
    }

}